{-# LANGUAGE OverloadedStrings #-}

module Data.ErrorMessage
  ( ErrorMessage (..)
  , MessageBlock (..)
  , renderErrorMessage
  , AsErrorMessage(..)
  , putErrorMessage
  ) where

import qualified Data.Text as T
import Prettyprinter
import Prettyprinter.Render.Terminal

data ErrorMessage = ErrorMessage {headerPrefix :: T.Text, header :: T.Text, messageBlocks :: [MessageBlock], messageBody :: T.Text, hints :: [T.Text]}

data MessageBlock = MessageBlock {position :: (Integer, Integer, T.Text), sourceLine :: T.Text, message :: T.Text}

renderErrorMessage :: ErrorMessage -> Doc AnsiStyle
renderErrorMessage e = group (annotate prefixStyle (pretty (headerPrefix e) <> colon) <+> pretty (header e))
                       <> Prettyprinter.line
                       <> indent 4 (vsep (renderBlock <$> messageBlocks e))
                       <> Prettyprinter.line
                       <> pretty (messageBody e)
                       <> Prettyprinter.line
                       <> vsep ((\h -> annotate hintStyle "Hint:" <+> pretty h) <$> hints e)
                       <> Prettyprinter.line
  where prefixStyle = bold <> color Red
        hintStyle = bold <> color Blue

renderBlock :: MessageBlock -> Doc AnsiStyle
renderBlock (MessageBlock (l, _, _) sl msg) = pretty l <+> align (pipe <+> group (pretty sl) <> Prettyprinter.line <> pipe <+> pretty msg)

class AsErrorMessage a where
  asErrorMessage :: a -> ErrorMessage

instance AsErrorMessage ErrorMessage where
  asErrorMessage = id

putErrorMessage :: AsErrorMessage a => a -> IO ()
putErrorMessage = putDoc . renderErrorMessage . asErrorMessage
