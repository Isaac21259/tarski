{-# LANGUAGE OverloadedStrings #-}

module Language.Tarski.Vernacular
  ( parseFile
  , ModuleName
  , Name (..)
  , Module (..)
  , Definition (..)
  , SyntacticTerm
  ) where

import qualified Data.Text as T
import Language.Tarski.Vernacular.Definition
import Text.Megaparsec.Error
import Language.Tarski.Foundation.Errors
import Language.Tarski.Foundation.Terms

parseFile :: FilePath -> IO (Either String Module)
parseFile f = do
  s <- readFile f
  case runModuleParser root f (T.pack s) of
    Left b -> return $ Left $ errorBundlePretty b
    Right m -> return $ return m

checkModule :: Module -> Either [TypeError] [HigherTerm]
checkModule m = checkModule' m

checkModule' :: Module -> Either [TypeError] [HigherTerm]
checkModule' (Module n es ds is) = do
  is'' <- is'
  _
  where is' = clean $ fmap checkModule' is
        clean l = case l of
                    [] -> Right []
                    (Left e:rs) -> case clean rs of
                                     Left es -> Left $ e:es
                                     Right ts -> Left [e]
                    (Right t:rs) -> case clean rs of
                                      Left es -> Left es
                                      Right ts -> Right $ t:ts
