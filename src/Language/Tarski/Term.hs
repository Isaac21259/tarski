{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}

module Language.Tarski.Term where
import qualified Data.Text as T
import Control.Monad.Hrolog
import Control.Monad
import GHC.Natural

type Name = [T.Text]

type UName s = HList s (HString s)

data TermF u v n a f = BottomF a
                     | AbsurdF a f
                     | TopF a
                     | UnitF a
                     | ElTopF a f
                     | TwoF a
                     | TtF a
                     | FfF a
                     | ElTwoF a f f
                     | UniverseF a u
                     | PiF a f f
                     | LambdaF a f
                     | ApplyF a f f
                     | SigmaF a f f
                     | ConsF a f f
                     | ElSigmaF a f
                     | VarF a v
                     | ExternalF a n
                     deriving Show

mapAll :: (u -> u') -> (v -> v') -> (n -> n') -> (a -> a') -> (f -> f') -> (TermF u v n a f -> TermF u' v' n' a' f')
mapAll _ _ _ af _ (BottomF a) = BottomF (af a)
mapAll _ _ _ af ff (AbsurdF a f) = AbsurdF (af a) (ff f)
mapAll _ _ _ af _ (TopF a) = TopF (af a)
mapAll _ _ _ af _ (UnitF a) = UnitF (af a)
mapAll _ _ _ af ff (ElTopF a f) = ElTopF (af a) (ff f)
mapAll _ _ _ af _ (TwoF a) = TwoF (af a)
mapAll _ _ _ af _ (TtF a) = TtF (af a)
mapAll _ _ _ af _ (FfF a) = FfF (af a)
mapAll _ _ _ af ff (ElTwoF a l r) = ElTwoF (af a) (ff l) (ff r)
mapAll uf _ _ af _ (UniverseF a u) = UniverseF (af a) (uf u)
mapAll _ _ _ af ff (PiF a l r) = PiF (af a) (ff l) (ff r)
mapAll _ _ _ af ff (LambdaF a f) = LambdaF (af a) (ff f)
mapAll _ _ _ af ff (ApplyF a l r) = ApplyF (af a) (ff l) (ff r)
mapAll _ _ _ af ff (SigmaF a l r) = SigmaF (af a) (ff l) (ff r)
mapAll _ _ _ af ff (ConsF a l r) = ConsF (af a) (ff l) (ff r)
mapAll _ _ _ af ff (ElSigmaF a f) = ElSigmaF (af a) (ff f)
mapAll _ vf _ af _ (VarF a v) = VarF (af a) (vf v)
mapAll _ _ nf af _ (ExternalF a n) = ExternalF (af a) (nf n)

instance Functor (TermF u v n a) where
  fmap = mapAll id id id id

traverseAll :: Applicative m => (u -> m u') -> (v -> m v') -> (n -> m n') -> (a -> m a') -> (f -> m f') -> (TermF u v n a f -> m (TermF u' v' n' a' f'))
traverseAll uf vf nf af ff tm = bubble $ mapAll uf vf nf af ff tm
  where bubble (BottomF a) = BottomF <$> a
        bubble (AbsurdF a f) = AbsurdF <$> a <*> f
        bubble (TopF a) = TopF <$> a
        bubble (UnitF a) = UnitF <$> a
        bubble (ElTopF a f) = ElTopF <$> a <*> f
        bubble (TwoF a) = TwoF <$> a
        bubble (TtF a) = TtF <$> a
        bubble (FfF a) = FfF <$> a
        bubble (ElTwoF a l r) = ElTwoF <$> a <*> l <*> r
        bubble (UniverseF a u) = UniverseF <$> a <*> u
        bubble (PiF a l r) = PiF <$> a <*> l <*> r
        bubble (LambdaF a f) = LambdaF <$> a <*> f
        bubble (ApplyF a l r) = ApplyF <$> a <*> l <*> r
        bubble (SigmaF a l r) = SigmaF <$> a <*> l <*> r
        bubble (ConsF a l r) = ConsF <$> a <*> l <*> r
        bubble (ElSigmaF a f) = ElSigmaF <$> a <*> f
        bubble (VarF a v) = VarF <$> a <*> v
        bubble (ExternalF a n) = ExternalF <$> a <*> n

instance (Unify s u, Unify s v, Unify s n, Unify s a, Unify s f, Free s a, Free s f, Free s u, Free s v, Free s n) => Unify s (TermF u v n a f) where
  (BottomF m1) `unify` (BottomF m2) = m1 === m2
  (AbsurdF m1 b1) `unify` (AbsurdF m2 b2) = m1 === m2 >> b1 === b2
  (TopF m1) `unify` (TopF m2) = m1 === m2
  (UnitF m1) `unify` (UnitF m2) = m1 === m2
  (ElTopF m1 b1) `unify` (ElTopF m2 b2) = m1 === m2 >> b1 === b2
  (TwoF m1) `unify` (TwoF m2) = m1 === m2
  (TtF m1) `unify` (TtF m2) = m1 === m2
  (FfF m1) `unify` (FfF m2) = m1 === m2
  (ElTwoF m1 l1 r1) `unify` (ElTwoF m2 l2 r2) = m1 === m2 >> l1 === l2 >> r1 === r2
  (UniverseF m1 t1) `unify` (UniverseF m2 t2) = m1 === m2 >> t1 === t2
  (PiF m1 l1 r1) `unify` (PiF m2 l2 r2) = m1 === m2 >> l1 === l2 >> r1 === r2
  (LambdaF m1 b1) `unify` (LambdaF m2 b2) = m1 === m2 >> b1 === b2
  (ApplyF m1 l1 r1) `unify` (ApplyF m2 l2 r2) = m1 === m2 >> l1 === l2 >> r1 === r2
  (SigmaF m1 l1 r1) `unify` (SigmaF m2 l2 r2) = m1 === m2 >> l1 === l2 >> r1 === r2
  (ConsF m1 l1 r1) `unify` (ConsF m2 l2 r2) = m1 === m2 >> l1 === l2 >> r1 === r2
  (ElSigmaF m1 b1) `unify` (ElSigmaF m2 b2) = m1 === m2 >> b1 === b2
  (VarF m1 t1) `unify` (VarF m2 t2) = m1 === m2 >> t1 === t2
  (ExternalF m1 t1) `unify` (ExternalF m2 t2) = m1 === m2 >> t1 === t2
  _ `unify` _ = mzero

type Metadata = Maybe ((Natural, Natural), (Natural, Natural))
type UMetadata s = HMaybe s (HTuple s (HTuple s (HNat s) (HNat s)) (HTuple s (HNat s) (HNat s)))

type Term u v n a = Fix (TermF u v n a)

type UTerm s = HFix s (TermF (HNat s) (HNat s) (UName s) (UMetadata s))

type BlankTerm = Term Natural Natural Name ()

type SyntacticTerm = Term Natural Natural Name (Maybe ((Natural, Natural), (Natural, Natural)))

instance HConvert s SyntacticTerm (UTerm s) where
  intoH (Fix f) = Other $ mapAll intoH intoH intoH intoH intoH f
  fromH (Other f) = Fix <$> traverseAll fromH fromH fromH fromH fromH f
  fromH (HVar v) = variable v
