{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Error
import Control.Monad
import Control.Monad.Except
import Control.Monad.State
import qualified Data.Text as T
import Language.Tarski.Vernacular
import Options.Applicative
import System.Console.Haskeline

data Args = Repl
          | Compile FilePath

replArgs :: Parser Args
replArgs = flag' Repl
              ( long "repl"
                <> short 'r'
                <> help "Open a repl"
              )

compileArgs :: Parser Args
compileArgs = Compile <$> strOption
              ( long "file"
                <> short 'f'
                <> metavar "FILENAME"
                <> help "Compile the file"
              )

args :: ParserInfo Args
args = info
       ( replArgs <|> compileArgs )
       ( fullDesc
         <> progDesc "The Tarski compiler"
         <> header "Tarski"
       )

-- repl :: IO ()
-- repl = void (runStateT (runInputT defaultSettings loop) ((Defined [], [0..], TypicalAmb [] []), 1))
--   where loop :: InputT (StateT ((Defined, [Integer], TypicalAmb), Integer) IO) ()
--         loop = do
--           ((ctx@(Defined vars), fresh, amb), line) <- lift get
--           minput <- getInputLine "tarski> "
--           case minput of
--             Nothing -> void (outputStrLn "Goodbye")
--             Just ":quit" -> void (outputStrLn "Goodbye")
--             Just ":vars" -> do
--               outputStrLn "Variables"
--               outputStrLn "-----------------"
--               mapM_ (\(n, tm, t) -> outputStrLn $ concat [show n, " = ", show tm, " : ", show t]) vars
--               outputStrLn ""
--               loop
--             Just input -> do
--               res <- evalErrorT $ parseRepl $ Textstream (Position line 0 0 "repl") $ T.pack input
--               case res of
--                 Just ds -> do
--                   (_, (ctx', fresh', amb')) <- runStateT (runDirectives ds) (ctx, fresh, amb)
--                   lift (put ((ctx', fresh', amb'), line + 1))
--                   loop
--                 Nothing -> loop

run :: Args -> IO ()
run Repl = error "Not Implemented"
run (Compile f) = do
  e <- parseFile f
  case e of
    Left s -> putStr s
    Right m -> print $ length $ definitions m

main :: IO ()
main = run =<< execParser args  
